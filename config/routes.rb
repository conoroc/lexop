Rails.application.routes.draw do

	devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
	resources :users, only: [:index, :destroy]

	resources :projects do
		get :search, on: :collection
	end

	resources :comments
	
	root to: "projects#index"
end
