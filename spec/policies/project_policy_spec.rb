require 'rails_helper'

describe ProjectPolicy do
  subject { described_class }

let(:user_admin) { User.create!(admin: true, name: 'Test', email: 'mail@mail.com', password: 'password') }
let(:user_one) { User.create!(admin: false, name: 'Test2', email: 'mail2@mail.com', password: 'password') }
let(:user_two) { User.create!(admin: false, name: 'Test3', email: 'mail3@mail.com', password: 'password') }
let(:project_one) { Project.create!(name: 'Test1', description: 'Lorem', project_type: 'Public', project_status: 'Created', estimated: 5, actual: 7, user_id: user_one.id, created_at: Time.now) }
let(:project_two) { Project.create!(name: 'Test2', description: 'Lorem', project_type: 'Private', project_status: 'Created', estimated: 5, actual: 7, user_id: user_one.id, created_at: Time.now) }

let(:scope) { Pundit.policy_scope!(user_one, project_one.class) }
  permissions :update?, :edit? do
    it "denies access if User does not own Project" do
      expect(subject).not_to permit(user_two, project_one)
    end

    it "allows access if User is Admin" do
      expect(subject).to permit(user_admin, project_one)
    end
  end

  permissions :scope do
    it "scope for non admin returns only Public Project" do
      expect(scope.length).to equal(1)
    end
  end
end