class UsersController < ApplicationController
  before_action :authenticate_user!
  def index
    @users = User.all
     authorize @users
  end

 def destroy
 	@user = User.find(params[:id])
    authorize @user
    @user.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def user_params
      params.require(:project).permit(:name, :description, :project_type, :estimated, :actual, :project_status, :user_id)
    end
end