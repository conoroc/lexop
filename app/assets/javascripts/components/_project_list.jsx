const ProjectList = createReactClass({
  render: function() {
    let projects = [];
    this.props.projects.forEach(function(project) {
      projects.push(<Project project={project} key={'project' + project.id}/>);
    }.bind(this));
    return (
      <div className="project-panels">
        {projects}
      </div>
        )
    }
});