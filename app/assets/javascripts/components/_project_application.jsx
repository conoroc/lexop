const ProjectApplication = createReactClass({
  getInitialState: function() {
    return { projects: [] };
  },
  componentDidMount: function() {
    this.setState({ projects: this.props.projects });
  },
  handleSearch: function(projects) {
    this.setState({ projects: projects });
  },
  render: function() {
    return(
      <div className="container">
        <section className="jumbotron text-center">
          <h1>Home Improvement App</h1>
          <p>by Conor O'Callaghan</p>
        </section>
        <section class="container">

         <div className="row">
        <div className="col-md-6 col-md-offset-3">
          <SearchForm handleSearch={this.handleSearch} />
        </div>
        </div>
        </section>
         <section class="container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
           <ProjectList projects={this.state.projects} />
          </div>
        </div>
        </section>
      </div>
    )
  }
});