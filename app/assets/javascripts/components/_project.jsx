const Project = createReactClass({
    propTypes: {
        name: PropTypes.string,
        description: PropTypes.string,
        project_type: PropTypes.string,
        project_status: PropTypes.string,
        estimated: PropTypes.string,
        actual: PropTypes.string,
        user_id: PropTypes.string,
        created_at: PropTypes.string,
    },

    _buildLinkHref(event) {
        return '/projects/' + this.props.project.id;
    },
    render: function() {
        const project = this.props.project;
        return (
            <div className="project-panel panel panel-default">
				<div className="panel-heading name-details">
					<div className="row">
						<div className="col-md-8">
							<div className="name-wrapper">
								{project.name}
								<a href={this._buildLinkHref()} className="btn btn-default">
								View Project
								</a>
							</div>
						</div>
						<div className="col-md-4">
							<div className="alert alert-success text-center project-status">{project.project_status}</div>
						</div>
					</div>
				</div>
				<div className="panel-body created-details">
					<div className="row">
						<div className="icons-wrapper">
							<span class="glyphicon glyphicon-time"></span>{project.created_time_ago} ago
							<span class="glyphicon glyphicon-user"></span>{project.project_owner}
						</div>
					</div>
				</div>
				<div className="panel-footer description-details">
					<div className="row">
						<div className="col-md-6">
							<p>{project.description}</p>
						</div>
						<div className="col-md-6">
							<div class="effort-wrapper">	
								<div className="well text-center">
									<span class="effort-title">Estimated Level of Effort</span>
									<span class="effort-number">{project.estimated}</span>
								</div>
								<div className="well text-center">
									<span class="effort-title">Actual Level of Effort</span>
									<span class="effort-number">{project.actual}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        )
    }
});