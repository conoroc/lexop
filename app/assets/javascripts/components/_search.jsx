const SearchForm = createReactClass({
  handleSearch: function() {
    const query = ReactDOM.findDOMNode(this.refs.query).value;
    const self = this;
    $.ajax({
      url: '/projects/search',
      data: { query: query },
      success: function(data) {
        self.props.handleSearch(data);
      },
      error: function(xhr, status, error) {
        alert('Search error: ', status, xhr, error);
      }
    });
  },
  render: function() {
    return(
      <input onChange={this.handleSearch}
             type="text"
             className="form-control"
             placeholder="Search project by name or description..."
             ref="query" />
    )
  }
}); 