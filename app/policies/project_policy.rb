class ProjectPolicy
  attr_reader :user, :project

  def initialize(user, project)
    @user = user
    @project = project
  end

  def index?
    true
  end

  def show?
    scope.where(:id => project.id).exists?
  end

  def create?
    true
  end

  def new?
    create?
  end

  def update?
    user.admin? or project.user_id == user.id
  end

  def edit?
    update?
  end

  def destroy?
    user.admin?
  end

  def scope
    Pundit.policy_scope!(user, project.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user && user.admin?
        scope.all
      else
        scope.where(project_type: 'Public')
      end
    end
  end
end