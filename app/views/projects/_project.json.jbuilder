json.extract! project, :id, :name, :description, :project_type, :estimated, :actual, :project_status, :created_at, :updated_at
json.url project_url(project, format: :json)
