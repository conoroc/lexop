class Project < ApplicationRecord

	include ActionView::Helpers::DateHelper

    TYPES = ['Public','Private']
    STATUSES = ['Created', 'Started', 'Stopped', 'Completed']

	belongs_to :user
	has_many :comments, dependent: :destroy

	validates :name, presence: true, uniqueness: true
	validates :description, presence: true, length: { in: 20..200 }
	validates :project_type, inclusion: { in: TYPES, message: "%{value} is not a valid type" }
  	validates :project_status, inclusion: { in: STATUSES, message: "%{value} is not a valid status" }
    validates :estimated, inclusion: { in: 1..10 }
    validates :actual, inclusion: { in: 1..10 }

    attribute :created_time_ago
    attribute :project_owner

    def created_time_ago
    	time_ago_in_words(self.created_at)
    end

    def project_owner
        return self.user.name
    end

end
