class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :description
      t.string :project_type
      t.string :project_status
      t.integer :estimated
      t.integer :actual
      t.references :user, foreign_key: true
      
      t.timestamps
    end

  end
end
